/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef CALDAVAKONADIPLUGIN_H
#define CALDAVAKONADIPLUGIN_H

#include <KAccountsDPlugin>

class KJob;

class CalDavAkonadiPlugin : public KAccountsDPlugin
{
    Q_OBJECT

public:
    CalDavAkonadiPlugin(QObject *parent, const QVariantList &args);

public Q_SLOTS:
    void onAccountCreated(const Accounts::AccountId accountId, const Accounts::ServiceList &serviceList) override;
    void onAccountRemoved(const Accounts::AccountId accountId) override;
    void onServiceEnabled(const Accounts::AccountId accountId, const Accounts::Service &service) override;
    void onServiceDisabled(const Accounts::AccountId accountId, const Accounts::Service &service) override;

private Q_SLOTS:
    void getCredentials(const Accounts::AccountId accountId);
    void getAccountDetails(KJob *job);
    void syncAccounts();
    void agentInstanceCreated(KJob *job);

private:
    void checkServices(const Accounts::AccountId accountId, const QUrl &server, const QString &userName, const QString &password);
    void createAkonadiResource(const QUrl &server, const QString &userName, const QString &password);
};

#endif // CALDAVAKONADIPLUGIN_H
