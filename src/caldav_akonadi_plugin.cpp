/*
 * SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "caldav_akonadi_plugin.h"

#include <QDebug>
#include <QTimer>
#include <QUrl>
#include <KJob>
#include <KPluginFactory>
#include <Accounts/Account>
#include <Accounts/AccountService>
#include <Accounts/Manager>
#include <Accounts/Service>
#include <Core>
#include <GetCredentialsJob>
#include <AkonadiCore/AgentInstanceCreateJob>
#include <AkonadiCore/AgentType>
#include <AkonadiCore/AgentManager>

K_PLUGIN_CLASS_WITH_JSON(CalDavAkonadiPlugin, "caldav_akonadi.json")

CalDavAkonadiPlugin::CalDavAkonadiPlugin(QObject *parent, const QVariantList &args)
    : KAccountsDPlugin(parent, args)
{
    syncAccounts();
}

void CalDavAkonadiPlugin::syncAccounts()
{
    const Accounts::AccountIdList accountList = KAccounts::accountsManager()->accountList(QStringLiteral("dav-calendars"));

    for (const quint32 accountId : accountList) {
        getCredentials(accountId);
    }
}

void CalDavAkonadiPlugin::onAccountCreated(const Accounts::AccountId accountId, const Accounts::ServiceList &serviceList)
{
    Accounts::Account *account = KAccounts::accountsManager()->account(accountId);

    if (!account) {
        return;
    }

    for (const Accounts::Service &service : serviceList) {
        if (service.serviceType() == QLatin1String("dav-calendars") && account->isEnabled()) {
            getCredentials(accountId);
        }
    }
}

void CalDavAkonadiPlugin::onServiceEnabled(const Accounts::AccountId accountId, const Accounts::Service &service)
{
    Accounts::Account *account = KAccounts::accountsManager()->account(accountId);

    Accounts::AccountService *accountService = new Accounts::AccountService(account, service);
    auto agentType = accountService->value("akonadi/resourceType").toString();
    qDebug() << "CalDavAkonadiPlugin: Enable service. Account Id:" << accountId << " Service type:" << service.serviceType() << "Agent Type:" << agentType;

    if (!account) {
        qWarning() << "Invalid account for id" << accountId;
        return;
    }

    if (service.serviceType() == QLatin1String("dav-calendars")) {
        getCredentials(accountId);
    }
}

void CalDavAkonadiPlugin::getCredentials(const Accounts::AccountId accountId)
{
    GetCredentialsJob *credentialsJob = new GetCredentialsJob(accountId, this);
    connect(credentialsJob, &GetCredentialsJob::finished, this, &CalDavAkonadiPlugin::getAccountDetails);
    credentialsJob->start();
}

void CalDavAkonadiPlugin::getAccountDetails(KJob *job)
{
    GetCredentialsJob *credentialsJob = qobject_cast<GetCredentialsJob *>(job);
    job->deleteLater();

    const QVariantMap &data = credentialsJob->credentialsData();
    Accounts::Account *account = KAccounts::accountsManager()->account(credentialsJob->accountId());
    auto host = account->value("dav/host").toString();

    if (host.isEmpty()) {
        qWarning() << "CalDavAkonadiPlugin: host is empty";
        return;
    }

    QUrl calDavUrl;
    calDavUrl.setHost(account->value("dav/host").toString());
    calDavUrl.setScheme("https");

    const QString &userName = data.value("AccountUsername").toString();
    const QString &password = data.value("Secret").toString();

    checkServices(credentialsJob->accountId(), calDavUrl, userName, password);
}

void CalDavAkonadiPlugin::onAccountRemoved(const Accounts::AccountId accountId)
{
    //TODO
    Q_UNUSED(accountId)
}

void CalDavAkonadiPlugin::onServiceDisabled(const Accounts::AccountId accountId, const Accounts::Service &service)
{
    //TODO
    Q_UNUSED(accountId)
    Q_UNUSED(service)
}

void CalDavAkonadiPlugin::checkServices(const Accounts::AccountId accountId, const QUrl &server, const QString &userName, const QString &password)
{
    Accounts::Account *account = KAccounts::accountsManager()->account(accountId);

    const Accounts::ServiceList services = account->services(QStringLiteral("dav-calendars"));
    qDebug() << "CalDavAkonadiPlugin:" << "# of services" << services.count();

    for (const auto &service : services) {
        account->selectService(service);

        QString resourceId = account->value("akonadi/resourceId").toString();
        auto isAkonadiCaldDav = account->value("akonadi/resourceType").toString() == QStringLiteral("akonadi-dav-calendar");

        if (isAkonadiCaldDav && resourceId.isEmpty()) {
            resourceId = QString("%1-%2-%3").arg("dav-calendars", server.host(), userName) ;
            qDebug() << "CalDavAkonadiPlugin:" << "resourceId is empty, we set it to: " << resourceId;
            account->setValue("akonadi/resourceId", resourceId);
            account->syncAndBlock();

            createAkonadiResource(server, userName, password);
        }
    }
};

void CalDavAkonadiPlugin::createAkonadiResource(const QUrl &server, const QString &userName, const QString &password)
{
    Akonadi::AgentType type = Akonadi::AgentManager::self()->type("akonadi_davgroupware_resource");
    Akonadi::AgentInstanceCreateJob *job = new Akonadi::AgentInstanceCreateJob(type);
    connect(job, SIGNAL(result(KJob*)),
            this, SLOT(agentInstanceCreated(KJob*)));
    job->start();

    // TODO: Do not trigger the configuration wizard. Resource should be automatically configured
    job->configure();
}

void CalDavAkonadiPlugin::agentInstanceCreated(KJob *job)
{
    Akonadi::AgentInstanceCreateJob *createJob = static_cast<Akonadi::AgentInstanceCreateJob *>(job);
    auto agentInstance = createJob->instance();
    qDebug() << "CalDavAkonadiPlugin: Agent instance created:" << createJob->instance().identifier();
}

#include "caldav_akonadi_plugin.moc"
