# SPDX-FileCopyrightText: 2021 Dimitris Kardarakos <dimkard@posteo.net>
#
# SPDX-License-Identifier: BSD-2-Clause

set(caldav_akonadi_plugin_SRCS
    caldav_akonadi_plugin.cpp
    )

kcoreaddons_add_plugin(caldav_akonadi_plugin
    JSON caldav_akonadi.json
    SOURCES ${caldav_akonadi_plugin_SRCS}
    INSTALL_NAMESPACE kaccounts/daemonplugins)

kde_enable_exceptions(caldav_akonadi_plugin)

target_link_libraries(
    caldav_akonadi_plugin
    Qt5::Core 
    KAccounts
    KAsync
    KF5::AkonadiCore
    KF5::ConfigCore
)
